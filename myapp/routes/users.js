var router = require('express').Router();
var models = require('../models');
var url    = require('url');
// var log    = require('../logger');

router.get('/', function (req, res) {
    var filter = {};
    var sort   = {};

    var params = {};

    if (req.query.name) filter.name = req.query.name;
    if (req.query.type) filter.type = req.query.type;

    if (req.query.sort) {
        var sortParams = req.query.sort.split('|');

        for (var i in sortParams) {
            var sortParam = sortParams[i].split(':');
            if (sortParam.length == 2) {
                var sortField = sortParam[0];
                var sortMode  = (parseInt(sortParam[1]) === -1) ? -1 : 1;
                sort[sortField] = sortMode;
            }
        }
    }

    var limit  = req.query.limit  || 10;
    var offset = req.query.offset || 0;

    limit = parseInt(limit);
    offset = parseInt(offset);

    models.users
      .find(filter)
      .limit(limit)
      .skip(offset)
      .sort(sort)
      .exec(function (err, results) {
          if (err) {
              log.error(err.message, err);
              return res.api.error(err);
          }

          params.data = results;
          params.title = "Users";

          return res.render('users/index', params);
      });
});

router.get('/create', function (req, res){
  var params = {
    'status_opts': ['PENDING','INTERVIEWED','REJECTED', 'HIRED']
  };
  return res.render('users/create', params);
});

router.post('/create', function (req, res) {
    var entity = new models.users(req.body);

    entity.save(function (err) {
        if (err) {
          console.log("may error")
            if (err.name === 'ValidationError') {
                return res.send({
                  'result' : 'ValidationError',
                  'message' : err
                });
            } else {
                return res.send({
                  'result' : 'error',
                  'message' : err
                });
            }
        }
        else{
          return res.redirect('/users');
        }
    });
});

router.get('/edit/:id', function (req, res, next) {
  var params = {};
  params.status_opts = ['PENDING','INTERVIEWED','REJECTED', 'HIRED'];
    models.users
      .find({
        "_id" : req.params.id
      })
      .exec(function (err, results) {
          if (err) {
              log.error(err.message, err);
              return res.api.error(err);
          }

          params.data = results[0];

          return res.render('users/edit', params);
          // return res.send(params);
      });
});

router.post('/edit/:id', function (req, res, next) {
  var updateParams = req.body;

	if (Object.keys(updateParams).length) {
		models.users.findByIdAndUpdate(
            req.params.id,
            updateParams,
			function (err, entity) {
				if (err) {
                    if (err.name === 'CastError') {
                        return next();
                    } else {
                        log.error(err.message, err);
                        return res.api.error(err);
                    }
                }

                if (entity === null) {
                    return next();
                }

          return res.redirect('/users');
			});
	} else {
        return res.send({
          "result": "error",
          "message": "Missing parameters."
        });
    }
});

router.get('/delete/:id', function (req, res, next) {
  var params = {};
  params.status_opts = ['PENDING','INTERVIEWED','REJECTED', 'HIRED'];
    models.users
      .find({
        "_id" : req.params.id
      })
      .exec(function (err, results) {
          if (err) {
              log.error(err.message, err);
              return res.api.error(err);
          }

          params.data = results[0];

          return res.render('users/delete', params);
      });
});

router.post('/delete/:id', function (req, res) {
    var userIds = [req.params.id];

    console.log("Delete ", userIds)

    if(userIds){
        if(userIds.length) {
            models.users.remove({ _id : { '$in' : userIds } },
                function (err) {
                    if (err) {
                        return res.send(err);
                    }
                    else{
                      return res.redirect('/users');
                    }
                   
                });
        }
        else {
          return res.send({
            "result": "error",
            "message": "Missing parameters."
          });
        }
    }
    else{
        return res.send({
          "result": "error",
          "message": "Missing parameters."
        });
    }

});

module.exports = router;
