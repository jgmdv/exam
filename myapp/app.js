var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require("./config");


// var models     = require('./models'); // Preload models
var mongoose   = require('mongoose');

/* - Mongo DB Connection - */

console.log("Connecting to Mongo DB...\nHost: "+config.mongodb.host+"\nDB: "+config.mongodb.db);
if(config.mongodb.options){
    console.log("Options:",config.mongodb.options);
}
else{
    console.log("No MongoDB options.");
}

var mongoConnect ='mongodb://' + config.mongodb.host + '/' + config.mongodb.db;
mongoConnect     = mongoConnect.trim();

console.log("URI", mongoConnect);
mongoose.connect(mongoConnect, config.mongodb.options);

mongoose.connection.on('error', function (err){
    // catches the error to prevent Source API downtime - just for testing purposes. not recommended for prod
    // remove this after successfully connecting to Replica Set
    console.log("Error:", err);
});

mongoose.connection.on('open', function () {
  console.log("mongodb connection open");
});


var index = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
