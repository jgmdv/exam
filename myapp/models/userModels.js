var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var schema = new Schema({
    first_name: {
        type: String
    },
    last_name: {
        type: String
    },
    status: {
        type: String,
        enum: ['PENDING','INTERVIEWED','REJECTED', 'HIRED'],
        default:'PENDING'
    }
});

schema.set('autoIndex', true);

module.exports = mongoose.model('users', schema);