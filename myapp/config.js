var config = {};

config.mongodb = {
    host      : '127.0.0.1',
    db        : 'examinees',
    secondary : '127.0.0.1'
};

module.exports = config;